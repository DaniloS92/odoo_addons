# -*- coding: utf-8 -*-
{
    'name': "app_courses",

    'summary': """
        Save data of courses""",

    'description': """
        Save and valiate data of courses
    """,

    'author': "Danilo Sanchez",
    'website': "http://www.danilosanchez.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Training',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        #'views/views.xml',
        #'views/templates.xml',
        'views/app_courses.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}