    # -*- coding: utf-8 -*-

from openerp import models, fields, api


class Courses(models.Model):
    _name = 'app.courses'
    _description = 'App Courses'

    name = fields.Char(
        string = 'Name',
        required = True,
        help = 'Insert name of course'
    )
    course_start_date = fields.Date(
        string = 'Start Date',
        required = True,
    )
    course_end_date = fields.Date(
        string = 'End Date',
        required = True,
    )
    course_limit = fields.Integer(
        string = 'Limit max students',
        default = 50,
        help = 'Max limit of students per course'
    )
    course_full = fields.Char(
        string=u'Course Percentage',
        readonly=True,
        help='Full course percentage',
        store=False,
        compute='_compute_percentage_course'
    )
    course_instructor_id = fields.Many2one( #un instructor puede tener muchos cursos
        'res.partner',
        string = 'Instructor'
    )
    course_student_ids = fields.Many2many( #un curso puede tener muchos estudiantes y un estudiante puede estudiar muchos cursos
        'res.partner',
        string = 'Students'
    )


    @api.depends('course_student_ids', 'course_limit')
    def _compute_percentage_course(self):
        """Calculo el porcentaje de cupos ocupados en el curso
        tomando en cuenta el limite de cupos disponible
        y la cantidad de estudiantes registrados
        """

        for record in self:
            number_students = len(record.course_student_ids)
            resp_percent = (number_students * 100) / record.course_limit
            record.course_full = "{}%".format(str(resp_percent))
    

    @api.constrains('course_start_date')
    def _check_date_start(self):
        """Valida si la fecha de inicio del curso es menor
        a la fecha del fin del mismo
        
        Raises:
            models.ValidationError -- Leventa un error si la fecha inicial el mayor a la final
            models.ValidationError -- Levanta un error si no hay fechas ingresadas
        """

        for record in self:
            date_start = record.course_start_date
            date_end = record.course_end_date
            if date_end and date_start:
                if date_start > date_end:
                    raise models.ValidationError('Start date > End Date')
            else:
                raise models.ValidationError('Enter Start date and End Date')
    

    @api.constrains('course_end_date')
    def _check_date_end(self):
        """Valida si la fecha de fin del curso es mayor
        a la fecha de inicio del mismo
        
        Raises:
            models.ValidationError -- Leventa un error si la fecha final es menor a la inicial
            models.ValidationError -- Levanta un error si no hay fechas ingresadas
        """
        for record in self:
            date_start = record.course_start_date
            date_end = record.course_end_date
            if date_end and date_start:
                if date_end < date_start:
                    raise models.ValidationError('End date < Start Date')
            else:
                raise models.ValidationError('Enter Start date and End Date')
    

    @api.constrains('course_student_ids')
    def _check_limit_course(self):
        """Valida si hay cupos disponibles
        
        Raises:
            models.ValidationError -- Levanta un error si ya los cupos disponibles estan llenos
            models.ValidationError -- Valida que un instructor no puede ser estudiante o viceversa
        """

        for record in self:
            number_students = len(record.course_student_ids)
            
            if record.course_limit < number_students:
                raise models.ValidationError('The number of students is greater than limit avaliable of the course')
            
            if record.course_instructor_id in record.course_student_ids:
                raise models.ValidationError('An Student can not be Instructor')
    

    @api.constrains('course_instructor_id')
    def _check_limit_course(self):
        """Valida si hay cupos disponibles
        
        Raises:
            models.ValidationError -- Valida que un instructor no puede ser estudiante o viceversa
        """

        for record in self:           
            if record.course_instructor_id in record.course_student_ids:
                raise models.ValidationError('An Instructor can not be Student')
    

    @api.model
    def create(self, vals):
        """Sobrecarga el metodo de guardado para que el nombre
        del curso se guarde siempre en mayusculas
        
        Arguments:
            vals {tupla} -- Valores que viene ingresados desde la vista
        
        Returns:
            model -- Modelo tipo curso
        """

        for k,v in vals.items():
            if k == 'name':
                v.upper()
                print v
        course = super(Courses, self).create(vals)
        return course


    @api.multi
    def get_course_name(self):
        """Devuele una lista con los nombres de todos los cursos
        
        Returns:
            strign -- Lista con todos los nombres de los cursos
        """
        courses = []
        for record in self:
            courses.append(record.name)
            print record
        resp = ', '.join(courses)

       # return resp
        return {'value':{},'success':{'title':'Info','message':resp}}
