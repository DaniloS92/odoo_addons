# -*- coding: utf-8 -*-

from openerp import models, fields

class Student(models.Model):
    _inherit = 'res.partner'

    student_ids = fields.Many2many(
        'app.courses', 'course_student_ids',
        string="Students of Courses")
    
    is_instructor = fields.Boolean(
        defautl=False,
        string="Is Instructor?"
    )
