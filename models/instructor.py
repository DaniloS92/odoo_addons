# -*- coding: utf-8 -*-

from openerp import models, fields

class Instructor(models.Model):
    _inherit = 'res.partner'

    instructor_ids = fields.One2many(
        'app.courses', 'course_instructor_id',
        string="Instructors of Courses")
    
    is_instructor = fields.Boolean(
        defautl=True,
        string="Is Instructor?"
    )
